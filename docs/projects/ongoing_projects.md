# Ongoing Projects

Listed here are the currently ongoing projects. (The list is not necessarily
exhaustive and will be updated regularly.)

## 1U/2U Structure

- [https://gitlab.com/librecube/prototypes/system-1u-structure](https://gitlab.com/librecube/prototypes/system-1u-structure)

The goal of this project is to develop a modular structure that fully conforms
to the CubeSat Design Specification and allows for housing of LibreCube boards.
If shall provide fixation points for solar panels and/or external structures
and shall be designed as such to allow a quick and easy removal of interior
boards without the need of structure disassembly.

## LibreSim

- [https://gitlab.com/librecube/prototypes/python-libresim](https://gitlab.com/librecube/prototypes/python-libresim)

This is a larger project to develop a simulator infrastructure that can be used
to simulate any kind of (space) mission. It is implemented in Python and follows
the recommendations provided in the ECSS Standard ECSS-E-ST-40-07C. The core
of LibreSim is a kernel to run simulations based on discrete-event
simulation (using SimPy), which has reached sufficient maturity.

The major part of work is now on the development of the model library.
The models of those libraries can then be used to assembly a system
to be simulated. For this, there are libraries for common models, that can be
used to model thermal behavior, dynamics, coordinate systems, electrical behavior,
and so on. The next higher layer are the generic models, that represent typical
systems used in space mission architectures, such as electrical power supply,
attitude control, and radio communications. They are defined in a generic way
and need to be adapted to simulate concrete physical systems.

Another important aspect of this project is visualization of the simulation output.
For, we are exploring the use of [Grafana](https://grafana.com/) with LibreSim
timeseries store and a [browser-based 3D viewer](https://gitlab.com/librecube/prototypes/python-webview3d).

## PLUTO

- [https://gitlab.com/librecube/prototypes/python-pluto](https://gitlab.com/librecube/prototypes/python-pluto)

This is a Python module for converting PLUTO scripts into native Python code.
PLUTO stands for Procedure Language for Users in Test and Operations and is a
domain specific language (DSL). It was developed by the European Cooperation
for Space Standardization for the automation of procedures in the development
and utilization of space systems (but not limited to it).
