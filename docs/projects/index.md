# Projects

Find here an overview on the past, current, and future projects of LibreCube.

Typically, projects start as [ideas](project_ideas.md) and are then developed as [prototypes](ongoing_projects.md). When the prototypes have matured and are
validated to work as intended, they are [published](completed_projects.md)
either as elements of the reference architecture (that is, ready-to-use
hardware and/or software products for space mission application)
or as software libraries, to be included in application developments.

The detailed status and activities related to project developments is available
at the [Kanban](https://tree.taiga.io/project/librecube-librecube/kanban).
