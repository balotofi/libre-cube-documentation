# Completed Projects

This page lists the LibreCube projects that are ready-to-use for your space mission.

## Power Control and Distribution Unit

- [https://gitlab.com/librecube/elements/LC2201A](https://gitlab.com/librecube/elements/LC2201A)

The Power Control and Distribution Unit (PCDU) provides uninterrupted electrical
power to attached systems. It outputs a stable, permanent 5 Volt line and four
switchable 5 Volt lines. Six connectors for solar panels are provided. Two
Li-Ion batteries can be mounted that are charged through the solar power input
and provide the system with power when the solar input is not available. The
entire system is designed as fail safe to ensure a robust and reliable power
supply.

## 1U/2U TestPOD

- [https://gitlab.com/librecube/elements/LC3201](https://gitlab.com/librecube/elements/LC3201)
- [https://gitlab.com/librecube/elements/LC3202](https://gitlab.com/librecube/elements/LC3202)

TestPODs are used as container during vibration and thermal testing of LibreCube satellites.
They adhere to the CubeSat PPOD specification, which is the de-facto standard
deployer specifications used for mounting CubeSats to the launch rocket.

## Flatsat

- [https://gitlab.com/librecube/elements/LC3101](https://gitlab.com/librecube/elements/LC3101)

The Flatsat is simply a large PCB with LibreCube connectors that allows to mount
LibreCube boards side-by-side to facilitate testing and debugging.

## Documents Store

- [https://gitlab.com/librecube/elements/LC6501](https://gitlab.com/librecube/elements/LC6501)

The documents store is a REST API database for storing large amounts of data.
It is designed for storing schemaless (NoSQL) data in the form of documents;
think of storing JSON data.
It may be used for storing large amounts of documents, whose data structure
may only be known during runtime and thus can be created on the fly.
The database is organized in domain/model categories.

## Timeseries Store

- [https://gitlab.com/librecube/elements/LC6502](https://gitlab.com/librecube/elements/LC6502)

The timeseries store is a REST API database for storing large amounts of
timeseries data. It may be used to store timestamped data,
such as computer metrics, satellite telemetry or IoT device measurements.
The database is organized in domain/parameter categories.

## Space Link Extension (SLE) Protocol

- [https://gitlab.com/librecube/lib/python-sle](https://gitlab.com/librecube/lib/python-sle)

Implementation of the CCSDS Space Link Extension (SLE) API in Python. It
provides the RAF (Return All Frames) and RCF (Return Channel Frames) Return Link
Services and the CLTU Forward Link Service.
The CCSDS Space Link Extension (SLE) services are used by all major space
agencies to interconnect ground stations to mission control systems.
This Python package implements the SLE User API and can be used to develop
SLE user and provider gateway applications on top of it.

## CCSDS File Delivery Protocol (CFDP)

- [https://gitlab.com/librecube/lib/python-cfdp](https://gitlab.com/librecube/lib/python-cfdp)

This Python module is an implementation of the CCSDS File Delivery Protocol.
The CFDP protocol provides reliable transfer of files from one endpoint to
another and has been designed to work well over space links that suffer
from outtakes and long delays. The basic operation of CFDP is to transfer a
file from a sender to a receiver (both referred to as CFDP entities).
 It can be used to perform  space to ground, ground to space,
space to space, and ground to ground file transfers.

## SpaceCAN

- [https://gitlab.com/librecube/lib/micropython-spacecan](https://gitlab.com/librecube/lib/micropython-spacecan)
- [https://gitlab.com/librecube/lib/mbed-spacecan](https://gitlab.com/librecube/lib/mbed-spacecan)

This are implementations of the SpaceCAN protocol for embedded systems. The
micropython implementation has as target board the pyboard, but it should run on
compatible boards as well. The C++ implementation utilizes the mbed framework.
