# Welcome to the LibreCube Documentation!

At LibreCube our vision is to enable everyone to get involved in building systems for exploration of near and remote locations using open source hardware and software. We believe that discovering new worlds and getting scientific insights should be a matter to all humankind.

Use the navigation menu on the left to learn more about the following:

- **Open Source**: All the software we use and everything we produce is open source.
- **Standards**: We rely on proven and tested standards for our system designs, with preference to standards from the space domain.
- **Reference Architecture**: Defining a generic architecture of system and modules that have standardized interfaces makes it possible to combine and reuse elements for various applications.
- **Community**: Everyone is welcome to join the LibreCube community and contribute to open source space exploration!
- **Projects**: Ongoing and complete projects.

The LibreCube website is here: [https://librecube.org](https://librecube.org)

If you like to contribute to this documentation, go to the [source code](https://gitlab.com/librecube/librecube.gitlab.io).

All documentation is licensed as [Attribution 4.0 International (CC BY 4.0)](https://creativecommons.org/licenses/by/4.0/)
