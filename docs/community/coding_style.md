# Coding Style

Code is read much more often than it is written. The guidelines provided here
are intended to improve the readability of code and make it consistent across
the wide spectrum of LibreCube projects.

A style guide is about consistency. Consistency with this style guide is
important. Consistency within a project is more important. Consistency within
one module or function is the most important.

## Python/MicroPython Coding Style

Please adhere to the coding style as defined in PEP8 ([https://pep8.org](https://pep8.org)).
We **strongly** recommend to add a linter to your IDE. For example use the
linter-flake8 module for the [Atom IDE](https://atom.io).

The guideline on documenting your Python code is here: [https://realpython.com/documenting-python-code/](https://realpython.com/documenting-python-code/)

A recall of the basic rules:

- Indentation is four spaces, don't use tabs
- Maximum line length is 79 characters
- Surround top-level function and class definitions with two blank lines.
- Method definitions inside a class are surrounded by a single blank line.

Here are some code examples for reference:

```python
import os
import sys


def long_function_name(
        var_one, var_two, var_three,
        var_four):
    print(var_one)


class FooBar:
    """This is the summary line

    This is the further elaboration of the docstring. Within this section,
    you can elaborate further on details as appropriate for the situation.
    Notice that the summary and the elaboration is separated by a blank new
    line.
    """

    def a_instance_method(self):
        """This is a quick summary line used as a description of the object."""
        # write some code here
        pass

    @classmethod
    def a_class_method(cls):
        # TODO: implement it
        pass


if (this_is_one_thing and
    that_is_another_thing):
    do_something()


my_list = [
    1, 2, 3,
    4, 5, 6,
    ]


result = some_function_that_takes_arguments(
    'a', 'b', 'c',
    'd', 'e', 'f',
)


with open('/path/to/some/file/you/want/to/read') as file_1, \
     open('/path/to/some/file/being/written', 'w') as file_2:
    file_2.write(file_1.read())


income = (gross_wages
          + taxable_interest
          + (dividends - qualified_dividends)
          - ira_deduction
          - student_loan_interest)
```

## C++ Coding Style

To be written...
