# Git Workflow

Provided here is the typical workflow we use for LibreCube projects.

## Forking

As first time contributor, you may first want to fork the LibreCube repository
that you want to contribute to into your private GitLab account. Use the fork
button in the project repository page for that and then clone to the repository
to your computer.

Also you need to set the upstream to remote in order to get any changes from the
repository. You only have to do this once:

```
$ git remote add upstream <path_to_librecube_repo>
```

Note that if you are member of the LibreCube project repository, you can directly
open new branches in the repository without the need to fork the project to your account.

## Branching

The `main` (formally called `master`) branch contains the latest version of the
project and is tested and runnable.

Urgent fixes may be applied directly to `main`.

The `develop` branch is taken from `main` and typically a bit ahead of `main`.
It is also tested and runnable and will be merged back into `main` at some point.

Think of the `develop` branch as being used for sprints. At the begin of a sprint,
feature and fixes branches are based off from `develop`. Eventually, they will
be merged into `develop` when completed.

The naming of your branch shall be `issue-XX`, using the number of the relevant
issue in the GitLab repository.

![](assets/git_branches.png)

## Commits

Write your commit messages in present tense and start with the verb describing
the type of commit. Only add a full stop "." if more than on sentence.

- Add something related to a (new) feature
- Fix some bug or other kind of problem
- Refactor some code but without effect on functionality
- Test something
- Document something
- Minor edit. For really subtle changes

Example:

```
Fix a wrong type conversion of sender's .emit() method
Add parameter dist to Entity class constructor
Test drop of frames during UDP transport
```

For minor changes, a simple comment `Minor` is sufficient.

## Rebasing

When working on your issue branch the develop branch may evolve due to other features and fixes being merged into develop. To take those changes into your branch use the git rebasing functionality. You may apply this every time before starting working on your branch. The typical workflow is as follows:

- Checkout develop:

```
$ git checkout develop
```

- Make sure develop is up to date:

```
$ git pull
```

- Checkout your branch:

```
$ git checkout issue-01
```

- Rebase with develop:

```
$ git rebase develop
```

- Alternatively, run the rebasing in an interactive way:

```
$ git rebase -i develop
```

- If merge issues are encountered (because conflicting changes were done in same files) you have to resolve them. Most IDEs provide a merge tools for this. Or use the command line:

```
$ git mergetool
```

- After fixing merge issues (if any) continue with rebase:

```
$ git rebase --continue
```

- If you make a mistake while merging, abert the merge to discard all changes you've made:

```
$ git rebase --abort
```

- Finally push changes to the remote repository. You need to force the push, since the commit history of the remote feature branch will be altered:

```
$ git push -f
```

## Merging

After completion of your issue branch, raise a merge request to have it
merged into `develop`, after being reviewed by one of the project owners.
