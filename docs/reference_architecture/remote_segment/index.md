# Remote Segment

![](assets/remote_segment_architecture_overview.png)


## Instruments

To be written...

## Platform

To be written...


### Power System

The Electrical Power System (EPS) ...

#### PCDU

- [https://gitlab.com/librecube/prototypes/system-pcdu](https://gitlab.com/librecube/prototypes/system-pcdu)

The Power Conditioning and Distribution Unit (PCDU) is in charge of processing
all the electrical power needed for operation of the spacecraft. It comprises
the following main functions:
- Condition the power coming from solar panels and batteries
- Manage and monitor the battery state of charge
- Distribute power within the spacecraft via protected and regulates lines
- Connect/disconnect battery
- Provide interface for telecommands and telemetry

## Ground Support Equipment

To be written...
